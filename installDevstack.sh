#!/bin/bash

### Script created by Stian Dalviken for installation of DevStack (OpenStack developer edition) ###

echo -e "\e[35m****** Updating, downloading and installing necessary dependencies, and setting up DevStack ****** \e[0m"
# Git needs to be installed to be able to download the devstack repository
sudo apt-get -y install git

echo -e "\e[35m****** Downloading DevStack to home directory ****** \e[0m"
# Reference: http://devstack.org/
git clone https://github.com/openstack-dev/devstack.git $HOME
echo -e "\e[32m****** Done ****** \e[0m"

echo -e "\e[35m****** Running DevStack installation script. This will take some time, approx 50 minutes, so please be patient ****** \e[0m"
cd $HOME/devstack; ./stack.sh
echo -e "\e[32m****** Done! Please see the above information about username and password, as well as web address ****** \e[0m"
