#!/bin/bash

### Script created by Stian Dalviken for installation of ownCloud and MSc project app ###

# Main reference for this manual installation is found here: http://doc.owncloud.org/server/6.0/admin_manual/installation/installation_source.html


# NOTE: MAKE SURE THIS SCRIPT IS EXECUTED IN THE SAME FOLDER AS WAS EXTRACTED TO, AS IT IS USING RELATIVE PATHS

# Setting up folder paths as variables
owncloudDir="$HOME/owncloud"
tempDir="owncloudTemp"
resourcesDir="resourcesOwncloud"


# \e[35m adds the green colour to the text to make it more visible, \e[0m resets the colour to default so that everything doesn't turn green in the terminal. \e[32m adds red colour to the text so that the user easily can see when tasks are completed.

echo -e "\e[35m****** Updating, downloading and installing necessary dependencies, and setting up ownCloud ****** \e[0m"
sudo apt-get update
echo -e "\e[35m****** Installing: openssh-server apache2 mysql-server libapache2-mod-php5 php5-gd php5-json php5-mysql php5-curl php5-intl php5-mcrypt php5-imagick phpmyadmin git ****** \e[0m"
sudo apt-get install -y openssh-server apache2 mysql-server libapache2-mod-php5 php5-gd php5-json php5-mysql php5-curl php5-intl php5-mcrypt php5-imagick phpmyadmin git
echo -e "\e[32m****** Done ****** \e[0m"

echo -e "\e[35m****** Checking to see if you already have an owncloud folder in your home directory ****** \e[0m"
if [ ! -d "$owncloudDir" ]; then
echo -e "\e[32m****** No owncloud folder found, proceeding with install and setup ****** \e[0m"

    # Creates a temp folder in home dir if it doesn't exist
    if [ ! -d "$tempDir" ]; then
        echo -e "\e[35m****** Creating temporary folder ****** \e[0m"
        mkdir "$tempDir"
        echo -e "\e[32m****** Done ****** \e[0m"
    fi
    
    echo -e "\e[35m****** Downloading ownCloud 6.0.4 to temp folder ****** \e[0m"
    wget -P $tempDir "https://download.owncloud.org/community/owncloud-6.0.4.tar.bz2"
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Extracting ownCloud 6.0.4 to home folder ****** \e[0m"
    tar -xjf "$tempDir/owncloud-6.0.4.tar.bz2" -C "$HOME"
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Downloading MSc project app for ownCloud to temp folder ****** \e[0m"
    # The below few lines is so that the user doesn't have to write yes when prompted if they want to add bitbucket.org to known hosts. Running this script several times will add more of the same lines to this file, so please make sure you only run this one time. It is intentially made to add the lines to the end of the file, as replacing the content of the file with those lines would overwrite any other lines in the file
    if [ ! -d "$HOME/.ssh" ]; then
        mkdir $HOME/.ssh
        chmod 700 $HOME/.ssh
    fi
    echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
    git clone https://stiandalviken@bitbucket.org/stiandalviken/mscprojectowncloud.git $tempDir/projectapp
    echo -e "\e[32m****** Done ****** \e[0m"
    
    echo -e "\e[35m****** Moving MSc project ownCloud app to $owncloudDir/apps/ ****** \e[0m"
    mv "$tempDir/projectapp" "$owncloudDir/apps/"
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Changing ownership of $owncloudDir folder to be owned by www-data (webserver Apache2) ****** \e[0m"
    sudo chown -R www-data:www-data "$owncloudDir"
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Creating a symbolic link in /var/www/html/ to the owncloud folder ****** \e[0m"
    # If there already is a file/link named owncloud in /var/www/html/ then delete this and create a new one to avoid any problems
    if [ -f "/var/www/html/owncloud" ]; then
        sudo rm "/var/www/html/owncloud"
    fi
    sudo ln -s "$owncloudDir" "/var/www/html/owncloud"
    sudo chown www-data:www-data "/var/www/html/owncloud"
    echo -e "\e[32m****** Done ****** \e[0m"

    # .htaccess won't work correctly without this. Reference: http://sharadchhetri.com/2014/01/17/htaccess-file-does-not-work-owncloud-6-in-ubuntu-13-10-server/
    echo -e "\e[35m****** Taking backup of original apache config file and copying new config file to enable reading of .htaccess ****** \e[0m"
    sudo cp "/etc/apache2/apache2.conf" "/etc/apache2/apache2.conf.BACKUP"
    sudo cp "$resourcesDir/apache2.conf" "/etc/apache2/"
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Taking backup of original apache ssl config file and copying new owncloud ssl config file ****** \e[0m"
    sudo cp "/etc/apache2/sites-available/default-ssl.conf" "/etc/apache2/sites-available/default-ssl.conf.BACKUP"
    sudo cp "$resourcesDir/default-ssl.conf" "/etc/apache2/sites-available/"
    echo -e "\e[32m****** Done ****** \e[0m"

    # Reference: http://www.akadia.com/services/ssh_test_certificate.html
    echo -e "\e[35m****** Creating private key and certificate for ownCloud. WebDAV requires this to work! ****** \e[0m"
    # Generating private key
    openssl genrsa -out $tempDir/owncloud.key 1024

    echo -e "\e[31m****** FILL THE FOLLOWING OUT WITH YOUR PREFERENCES, BUT FOR 'Common Name (e.g. server FQDN or YOUR name) []:' YOU HAVE TO ENTER THE IP/HOSTNAME OF YOUR SERVER e.g. 192.168.1.100 (for local installation) OR www.your-server.com (if installed on a public web address) ****** \e[0m"
    currentIP=$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
    echo -e "\e[36m****** Your current local IP (eth0 inet addr) is: $currentIP ****** \e[0m"
    # Generating a CSR (Certificate Signing Request)
    openssl req -new -key $tempDir/owncloud.key -out $tempDir/owncloud.csr

    # Removing Passphrase from Key
    cp $tempDir/owncloud.key $tempDir/owncloud.key.org
    openssl rsa -in $tempDir/owncloud.key.org -out $tempDir/owncloud.key

    # Generating a Self-Signed Certificate
    openssl x509 -req -days 365 -in $tempDir/owncloud.csr -signkey $tempDir/owncloud.key -out $tempDir/owncloud.crt
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Moving private key and certificate to appropriate location ****** \e[0m"
    sudo mv $tempDir/owncloud.crt /etc/ssl/certs/
    sudo mv $tempDir/owncloud.key /etc/ssl/private/
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Enabling apache mods: rewrite (for .htaccess) and ssl ****** \e[0m"
    sudo a2enmod rewrite
    sudo a2enmod ssl
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Disabling default apache http config to avoid confusion with https ****** \e[0m"
    sudo a2dissite "000-default.conf"
    echo -e "\e[32m****** Done ****** \e[0m"

    echo -e "\e[35m****** Enabling apache ssl config for owncloud, and restarting apache ****** \e[0m"
    sudo a2ensite default-ssl
    sudo service apache2 restart
    echo -e "\e[32m****** Done ****** \e[0m"


    echo -e "\e[35m****** Cleaning up temporary files and folders ****** \e[0m"
    rm -R "$tempDir"
    echo -e "\e[32m****** Everything cleaned up and the installation and setup is complete ****** \e[0m"
    echo -e "\e[32m****** You can now go to https://[YOUR-IP]/owncloud to continue the installation of ownCloud ****** \e[0m"

else
    echo -e "\e[35m****** You already have an owncloud folder in your home directory, please remove this folder if you want to reinstall owncloud ****** \e[0m"
fi
